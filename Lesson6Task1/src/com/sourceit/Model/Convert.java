package com.sourceit.Model;

public interface Convert {

    float getCourseBuyCurrency(float currency, String currencyName);

    float getCourseSellCurrency(float currency, String currencyName);


}
