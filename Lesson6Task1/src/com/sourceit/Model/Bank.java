package com.sourceit.Model;

public class Bank extends Finance implements Sender, Credit, Deposit, Convert {

    private int yearOfLicense;

    final static float maxAmountOfCurrencyExchange = 12000f;
    final static float commissionForCurrencyExchange = 15f;

    final static String USD = "USD";
    final static String EUR = "EUR";
    final static String RUR = "RUR";

    final static float courseBuyUsd = 25.2f;
    final static float courseSellUsd = 24.2f;
    final static float courseBuyEur = 30.0f;
    final static float courseSellEur = 29.0f;
    final static float courseBuyRur = 0.45f;
    final static float courseSellRur = 0.4f;

    final static float maxAmountOfCredit = 200000f;
    final static float percentCredit = 25f;

    final static float percentDeposit = 20f;
    final static int maxTermOfDeposit = 12;


    final static float percentForSend = 1f;
    final static float additionalCommission = 5f;


    public Bank(String name, String address, int yearOfLicense) {
        super(name, address);
        this.yearOfLicense = yearOfLicense;
    }

    @Override
    public String toString() {
        return "Finance organization: " + this.name +
                ". Address: " + this.address +
                ". YearOfLicense: " + this.yearOfLicense +
                ". MaxAmountOfCurrencyExchange=" + maxAmountOfCurrencyExchange +
                ", \ncommissionForCurrencyExchange=" + commissionForCurrencyExchange +
                ". CourseBuyUsd = " + courseBuyUsd +
                ", courseSellUsd = " + courseSellUsd +
                ", courseBuyEur = " + courseBuyEur +
                ", courseSellEur = " + courseSellEur+
                ", \ncourseBuyRur = " + courseBuyRur +
                ", courseSellRur = " + courseSellRur+
                ", maxAmountOfCredit=" + maxAmountOfCredit +
                ", percentCredit=" + percentCredit +
                ", percentDeposit=" + percentDeposit +
                ", \nmaxTermOfDeposit=" + maxTermOfDeposit +
                ", percentForSend=" + percentForSend +
                ", additionalCommission=" + additionalCommission + '.';
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public float getCommissionSend(float uah) {
        float commission = uah * percentForSend / 100 + additionalCommission;
        return commission;
    }

    @Override
    public float getInterestOnDeposits(float uah, int month) {
        if (month <= maxTermOfDeposit) {
            float interestOnDeposits = (uah * percentDeposit / 12 / 100) * month;
            return interestOnDeposits;
        } else {
            return 0;
        }
    }

    @Override
    public float getMinPercentCredit(float uah) {
        if (uah <= maxAmountOfCredit) {
            return percentCredit;
        } else {
            return 0;
        }
    }

    @Override
    public float getCourseBuyCurrency(float currency, String currencyName) {
        if (currency > maxAmountOfCurrencyExchange) {
            return 0;
        }
        if (currencyName.equalsIgnoreCase(USD)) {
            float amountCurrency = (currency - commissionForCurrencyExchange) / courseBuyUsd;
            return amountCurrency;
        } else if (currencyName.equalsIgnoreCase(EUR)) {
            float amountCurrency = (currency - commissionForCurrencyExchange) / courseBuyEur;
            return amountCurrency;
        } else if (currencyName.equalsIgnoreCase(RUR)) {
            float amountCurrency = (currency - commissionForCurrencyExchange) / courseBuyRur;
            return amountCurrency;
        } else {
            return 0;
        }
    }

    @Override
    public float getCourseSellCurrency(float currency, String currencyName) {

        if (currencyName.equalsIgnoreCase(USD) && currency * courseSellUsd <= maxAmountOfCurrencyExchange) {
            float amountCurrency = currency * courseSellUsd - commissionForCurrencyExchange;
            return amountCurrency;
        } else if (currencyName.equalsIgnoreCase(EUR) && currency * courseSellEur <= maxAmountOfCurrencyExchange) {
            float amountCurrency = currency * courseSellEur - commissionForCurrencyExchange;
            return amountCurrency;
        } else if (currencyName.equalsIgnoreCase(RUR) && currency * courseSellRur <= maxAmountOfCurrencyExchange) {
            float amountCurrency = currency * courseSellRur - commissionForCurrencyExchange;
            return amountCurrency;
        } else {
            return 0;
        }
    }
    @Override
    public int compareTo(Finance o) {
        return this.name.compareTo(o.name);
    }

}
