package com.sourceit.Model;

public class Pif extends Finance implements Deposit {

    private int yearOfFoundation;
    final static int minTermOfDeposit = 12;
    final static float percentDeposit = 25f;

    public Pif(String name, String address, int yearOfFoundation) {
        super(name, address);
        this.yearOfFoundation = yearOfFoundation;

    }

    @Override
    public String toString() {
        return "Finance organization: " + this.name +
                ". Address: " + this.address +
                ". YearOfFoundation: " + this.yearOfFoundation +
                ". \nMinTermOfDeposit=" + minTermOfDeposit +
                ", percentDeposit=" + percentDeposit;
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public float getInterestOnDeposits(float uah, int month) {
        if (month >= minTermOfDeposit) {
            float interestOnDeposits = (uah * percentDeposit / 12 / 100) * month;
            return interestOnDeposits;
        } else {
            return 0;
        }
    }

    @Override
    public int compareTo(Finance o) {
        return this.name.compareTo(o.name);
    }
}
