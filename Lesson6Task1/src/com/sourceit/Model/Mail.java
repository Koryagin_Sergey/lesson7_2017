package com.sourceit.Model;

public class Mail extends Finance implements Sender {

    final static float percentForSend = 2f;

    public Mail(String name, String address) {
        super(name, address);


    }

    @Override
    public String toString() {
        return "Finance organization: " +
                name + ". Address: " + address +
                ". Percent on transfer: " + percentForSend + "% from the sum.";
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public float getCommissionSend(float uah) {
        float commission = uah * percentForSend / 100;
        return commission;
    }

    @Override
    public int compareTo(Finance o) {
        return this.name.compareTo(o.name);
    }
}
