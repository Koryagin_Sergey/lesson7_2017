package com.sourceit.Model;

public abstract class Finance implements Comparable<Finance> {

    String name;
    String address;

    public Finance(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public abstract void print();

}
