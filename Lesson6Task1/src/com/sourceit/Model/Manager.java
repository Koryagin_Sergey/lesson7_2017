package com.sourceit.Model;

import java.util.ArrayList;
import java.util.Collections;

public class Manager {

//    private Finance [] organizations;

    private ArrayList<Finance> organizations;

    public Manager() {
        this.update();
    }

    private void update() {
//        this.organizations = new Finance[7];
//
//        organizations[0] = new Mail("Mail", "Kharkov");
//        organizations[1] = new Bank("Privat", "Dnepr", 1990);
//        organizations[2] = new Pif("PIF", "Kyiv", 1900);
//        organizations[3] = new Lombard("Lombard", "Poltava");
//        organizations[4] = new CreditCafe("Credit cafe", "Ashan");
//        organizations[5] = new CreditUnion("Credit union", "Odessa");
//        organizations[6] = new Exchange("Roza","Kharkov");

        this.organizations = new ArrayList<>();

        Finance finance = new Mail("Mail", "Kharkov");
        this.organizations.add(finance);
        finance = new Bank("Privat", "Dnepr", 1990);
        this.organizations.add(finance);
        finance = new Pif("PIF", "Kyiv", 1900);
        this.organizations.add(finance);
        finance = new Lombard("Lombard", "Poltava");
        this.organizations.add(finance);
        finance = new CreditCafe("Credit cafe", "Ashan");
        this.organizations.add(finance);
        finance = new CreditUnion("Credit union", "Odessa");
        this.organizations.add(finance);
        finance = new Exchange("Roza", "Kharkov");
        this.organizations.add(finance);

    }

    public Finance getOrganizationWithMinPercentCredit(float uah) {
        Finance retVal = null;
        for (Finance fin : this.organizations) {
            if (fin instanceof Credit) {
                if (((Credit) fin).getMinPercentCredit(uah) == 0) {
                    continue;
                }
                if (retVal == null) {
                    retVal = fin;
                }

                if (((Credit) fin).getMinPercentCredit(uah) < ((Credit) retVal).getMinPercentCredit(uah)) {
                    retVal = fin;
                }
            }
        }

        return retVal;
    }

    public Finance getOrganizationWithMaxDeposit(float uah, int month) {
        Finance retVal = null;
        for (Finance fin : this.organizations) {
            if (fin instanceof Deposit) {
                if (((Deposit) fin).getInterestOnDeposits(uah, month) == 0) {
                    continue;
                }
                if (retVal == null) {
                    retVal = fin;
                }

                if (((Deposit) fin).getInterestOnDeposits(uah, month) > ((Deposit) retVal).getInterestOnDeposits(uah, month)) {
                    retVal = fin;
                }
            }
        }

        return retVal;
    }

    public Finance getOrganizationWithBestSender(float uah) {
        Finance retVal = null;
        for (Finance fin : this.organizations) {
            if (fin instanceof Sender) {
                if (((Sender) fin).getCommissionSend(uah) == 0) {
                    continue;
                }
                if (retVal == null) {
                    retVal = fin;
                }

                if (((Sender) fin).getCommissionSend(uah) < ((Sender) retVal).getCommissionSend(uah)) {
                    retVal = fin;
                }
            }
        }

        return retVal;
    }

    public Finance getBestCourseBuyCurrency(float currency, String currencyName) {
        Finance retVal = null;
        for (Finance fin : this.organizations) {
            if (fin instanceof Convert) {
                if (((Convert) fin).getCourseBuyCurrency(currency, currencyName) == 0) {
                    continue;
                }
                if (retVal == null) {
                    retVal = fin;
                }

                if (((Convert) fin).getCourseBuyCurrency(currency, currencyName) > ((Convert) retVal).getCourseBuyCurrency(currency, currencyName)) {
                    retVal = fin;
                }
            }
        }

        return retVal;
    }

    public Finance getBestCourseSellCurrency(float currency, String currencyName) {
        Finance retVal = null;
        for (Finance fin : this.organizations) {
            if (fin instanceof Convert) {
                if (((Convert) fin).getCourseSellCurrency(currency, currencyName) == 0) {
                    continue;
                }
                if (retVal == null) {
                    retVal = fin;
                }

                if (((Convert) fin).getCourseSellCurrency(currency, currencyName) > ((Convert) retVal).getCourseSellCurrency(currency, currencyName)) {
                    retVal = fin;
                }
            }
        }

        return retVal;
    }

    public void printSortedInfo() {
        Collections.sort(this.organizations);
        for (Finance finance : organizations) {
            finance.print();
        }
    }

}
