package com.sourceit.Model;

public interface Deposit {

    float getInterestOnDeposits(float uah, int month);

}
