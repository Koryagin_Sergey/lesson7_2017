package com.sourceit.Model;

public class Exchange extends Finance implements Convert {

    final static String USD = "USD";
    final static String EUR = "EUR";

    final static float courseBuyUsd = 27.2f;
    final static float courseSellUsd = 26.2f;
    final static float courseBuyEur = 32.0f;
    final static float courseSellEur = 31.0f;

    public Exchange(String name, String address) {
        super(name, address);
    }

    @Override
    public String toString() {
        return "Finance organization: " + this.name +
                ". Address: " + this.address +
                ". CourseBuyUsd = " + courseBuyUsd +
                ", courseSellUsd = " + courseSellUsd +
                ", courseBuyEur = " + courseBuyEur +
                ", courseSellEur = " + courseSellEur + ".";
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public float getCourseBuyCurrency(float currency, String currencyName) {
        if (currencyName.equalsIgnoreCase(USD)) {
            float amountCurrency = currency / courseBuyUsd;
            return amountCurrency;
        } else if (currencyName.equalsIgnoreCase(EUR)) {
            float amountCurrency = currency / courseBuyEur;
            return amountCurrency;
        } else {
            return 0;
        }
    }

    @Override
    public float getCourseSellCurrency(float currency, String currencyName) {
        if (currencyName.equalsIgnoreCase(USD)) {
            float amountCurrency = currency * courseSellUsd;
            return amountCurrency;
        } else if (currencyName.equalsIgnoreCase(EUR)) {
            float amountCurrency = currency * courseSellEur;
            return amountCurrency;
        } else {
            return 0;
        }
    }

    @Override
    public int compareTo(Finance o) {
        return this.name.compareTo(o.name);
    }

}
