package com.sourceit.Model;

public class CreditUnion extends Finance implements Credit {

    final static float maxAmountOfCredit = 100000f;
    final static float percentCredit = 20f;

    public CreditUnion(String name, String address) {
        super(name, address);
    }

    @Override
    public String toString() {
        return "Finance organization: " + this.name +
                ". Address: " + this.address +
                ". MaxAmountOfCredit=" + maxAmountOfCredit +
                ", percentCredit=" + percentCredit;
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public float getMinPercentCredit(float uah) {
        if (uah <= maxAmountOfCredit) {
            return percentCredit;
        } else {
            return 0;
        }
    }

    @Override
    public int compareTo(Finance o) {
        return this.name.compareTo(o.name);
    }

}
