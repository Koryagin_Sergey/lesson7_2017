package com.sourceit;

import java.util.Scanner;

import com.sourceit.Model.*;

public class Main {

    public static void main(String[] args) {

        Manager manager = new Manager();

        Scanner scan = new Scanner(System.in);


        System.out.println("Enter credit amount: ");
        float moneyInCredit = Float.parseFloat(scan.nextLine());

        Finance credit = manager.getOrganizationWithMinPercentCredit(moneyInCredit);
        if (credit == null) {
            System.out.println("Credit impossible.");
        } else {
            credit.print();
        }

        System.out.println("Enter deposit amount: ");
        float moneyOnDeposit = Float.parseFloat(scan.nextLine());
        System.out.println("Enter deposit term: ");
        int termOfDeposit = Integer.parseInt(scan.nextLine());

        Finance deposit = manager.getOrganizationWithMaxDeposit(moneyOnDeposit, termOfDeposit);
        if (deposit == null) {
            System.out.println("Deposit impossible.");
        } else {
            deposit.print();
        }

        System.out.println("Enter amount to send: ");
        float moneyForSend = Float.parseFloat(scan.nextLine());

        Finance sender = manager.getOrganizationWithBestSender(moneyForSend);
        sender.print();

        System.out.println("Enter amount UAH for buy currency: ");
        float amountForBuyCurrency = Float.parseFloat(scan.nextLine());
        System.out.println("Enter name currency for buy: ");
        String nameCurrencyForBuy = scan.nextLine();

        Finance convertBuy = manager.getBestCourseBuyCurrency(amountForBuyCurrency, nameCurrencyForBuy);
        if (convertBuy == null) {
            System.out.println("Convert impossible.");
        } else {
            convertBuy.print();
        }

        System.out.println("Enter name currency for sell: ");
        String nameCurrencyForSell = scan.nextLine();
        System.out.println("Enter amount currency for sell: ");
        float amountForSellCurrency = Float.parseFloat(scan.nextLine());

        Finance convertSell = manager.getBestCourseSellCurrency(amountForSellCurrency, nameCurrencyForSell);
        if (convertSell == null) {
            System.out.println("Convert impossible.");
        } else {
            convertSell.print();
        }
        System.out.println();
        System.out.println("List of organizations.");
        System.out.println();

        manager.printSortedInfo();


    }
}

